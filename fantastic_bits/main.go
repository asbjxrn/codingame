package main

import (
	"fmt"
	"math"
	"os"
	"sort"
)

/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/

const (
	MAXENTITIES = 13
)

type Position struct {
	X, Y int
}

type Speed struct {
	Vx, Vy int
}

type Entity struct {
	Position
	Speed
	Friction float64
	Radius   int
}

type EntityList []int

type Snaffle Entity

type Bludger Entity

type Wizard struct {
	Entity
	Id        int
	State     int
	OEntity   int
	OCoolDown int
	AEntity   int
	ACoolDown int
	FEntity   int
	FCoolDown int
	ThrewLast bool
	ThrownId  int
}

type World struct {
	MP          int
	MyGoal      Position
	OppGoal     Position
	Entities    [MAXENTITIES]Entity
	SnaffleIds  EntityList
	BludgerIds  EntityList
	MeIds       EntityList
	Me          [MAXENTITIES]Wizard
	OpponentIds EntityList
	Opponent    [MAXENTITIES]Wizard
	PetriLast   EntityList
}

type By func(e1, e2 int) bool

func (by By) Sort(entities []int) {
	es := &entitySorter{
		entities: entities,
		by:       by,
	}
	sort.Sort(es)
}

type entitySorter struct {
	entities []int
	by       func(e1, e2 int) bool
}

func (s *entitySorter) Len() int {
	return len(s.entities)
}

func (s *entitySorter) Swap(i, j int) {
	s.entities[i], s.entities[j] = s.entities[j], s.entities[i]
}

func (s *entitySorter) Less(i, j int) bool {
	return s.by(s.entities[i], s.entities[j])
}

func Distance(p1, p2 Position) int {
	return int(math.Sqrt((float64(p1.X-p2.X) * float64(p1.X-p2.X)) +
		(float64(p1.Y-p2.Y) * float64(p1.Y-p2.Y))))
}

func (e EntityList) SortByDistTo(p Position, world *World) {
	distance := func(s1, s2 int) bool {
		return Distance(p, world.Entities[s1].Position) < Distance(p, world.Entities[s2].Position)
	}
	By(distance).Sort(e)
}

func (e EntityList) SortByNextDistTo(p Position, world *World) {
	distance := func(s1, s2 int) bool {
		return Distance(p, NextPos(world.Entities[s1], world)) < Distance(p, NextPos(world.Entities[s2], world))
	}
	By(distance).Sort(e)
}

func (e EntityList) SortByEndDistTo(p Position, world *World) {
	distance := func(s1, s2 int) bool {
		return Distance(p, EndPos(world.Entities[s1])) < Distance(p, EndPos(world.Entities[s2]))
	}
	By(distance).Sort(e)
}

func EndPos(e Entity) Position {
	return Position{e.X + 4*e.Vx, e.Y + 4*e.Vy}
}

func NextPos(e Entity, world *World) Position {
	return Position{e.X + e.Vx, e.Y + e.Vy}
}

func AddX(x int, p Position) Position {
	return Position{p.X + x, p.Y}
}

func AddY(y int, p Position) Position {
	return Position{p.X, p.Y + y}
}

func ClosestSnaffle(p Position, world *World) int {
	minDist := 160000
	target := world.SnaffleIds[0]
	for _, id := range world.SnaffleIds {
		dist := Distance(p, world.Entities[id].Position)
		if dist < minDist {
			minDist = dist
			target = id
		}
	}
	return target
}

func Angle(p1, p2 Position) float64 {
	return float64(p1.Y-p2.Y) / float64(p1.X-p2.X)
}

func AimAtGoal(p1, p2, goal Position, radius int, world *World) bool {
	if (p1.X < p2.X && p1.X > goal.X) ||
		(p1.X > p2.X && p1.X < goal.X) {
		return false
	}
	angle := Angle(p1, p2)
	if goal.X < p1.X {
		return angle < Angle(p1, Position{goal.X, goal.Y - radius}) &&
			angle > Angle(p1, Position{goal.X, goal.Y + radius})
	}
	return angle < Angle(p1, Position{goal.X, goal.Y + radius}) &&
		angle > Angle(p1, Position{goal.X, goal.Y - radius})
}

func sign(p1, p2, p3 Position) float64 {
	return float64((p1.X-p3.X)*(p2.Y-p3.Y)) - float64((p2.X-p3.X)*(p1.Y-p3.Y))
}

func PointInTriangle(p, a, b, c Position) bool {
	b1 := sign(p, a, b) < 0.0
	b2 := sign(p, b, c) < 0.0
	b3 := sign(p, c, a) < 0.0
	return (b1 == b2) && (b2 == b3)
}

func Colliding(e1, e2 Entity) bool {
	if e1.Vx-e2.Vx == 0 {
		return false
	}
	xtdist := (e2.X - e1.X) / (e1.Vx - e2.Vx)
	if xtdist < 0 {
		return false
	}
	y1 := e1.Y + xtdist*e1.Vy
	y2 := e2.Y + xtdist*e2.Vy
	if (y1 < y2 && y2-y1 < e1.Radius+e2.Radius) || (y1 > y2 && y1-y2 < e1.Radius+e2.Radius) {
		return true
	}
	return false
}

type Action interface {
	String() string
	Apply(w *World) *World
}

type Move struct {
	WizardId int
	Position
	Thrust int
}

func (m Move) String() string {
	return fmt.Sprintf("MOVE %d %d %d\n", m.X, m.Y, m.Thrust)
}

func (m Move) Apply(w *World) *World {
	fmt.Printf("%v", m)
	w.Me[m.WizardId].ThrewLast = false
	return w
}

type Accio struct {
	WizardId int
	Entity   int
}

func (a Accio) String() string {
	return fmt.Sprintf("ACCIO %d\n", a.Entity)
}

func (a Accio) Apply(w *World) *World {
	fmt.Printf("%v", a)
	w.MP -= 20
	w.Me[a.WizardId].ACoolDown = 6
	w.Me[a.WizardId].AEntity = a.Entity
	w.Me[a.WizardId].ThrewLast = false
	return w
}

type Flipendo struct {
	WizardId int
	Entity   int
}

func (f Flipendo) String() string {
	return fmt.Sprintf("FLIPENDO %d\n", f.Entity)
}

func (f Flipendo) Apply(w *World) *World {
	fmt.Printf("%v", f)
	w.MP -= 20
	w.Me[f.WizardId].FCoolDown = 3
	w.Me[f.WizardId].FEntity = f.Entity
	w.Me[f.WizardId].ThrewLast = false
	return w
}

type Oblivate struct {
	WizardId int
	Entity   int
}

func (o Oblivate) String() string {
	return fmt.Sprintf("OBLIVATE %d\n", o.Entity)
}

func (o Oblivate) Apply(w *World) *World {
	fmt.Printf("%v", o)
	w.MP -= 5
	w.Me[o.WizardId].OCoolDown = 3
	w.Me[o.WizardId].ThrewLast = false
	return w
}

type Throw struct {
	SnaffleId int
	WizardId  int
	Position
	Force int
}

func (t Throw) String() string {
	return fmt.Sprintf("THROW %d %d %d\n", t.X, t.Y, 500)
}

func (t Throw) Apply(w *World) *World {
	fmt.Printf("%v", t)
	w.Me[t.WizardId].ThrewLast = true
	w.Me[t.WizardId].ThrownId = t.SnaffleId
	return w
}

type Petrificus struct {
	Entity   int
	WizardId int
}

func (p Petrificus) String() string {
	return fmt.Sprintf("PETRIFICUS %d\n", p.Entity)
}

func (p Petrificus) Apply(w *World) *World {
	fmt.Printf("%v", p)
	w.MP -= 10
	w.Me[p.WizardId].ThrewLast = false
	w.PetriLast = append(w.PetriLast, p.Entity)
	return w
}

type Strategy func(world *World) []Action

func StopMoving(wizardId int, world *World) Action {
	fmt.Fprintf(os.Stderr, "Wizard %d: Stopping\n", wizardId)
	return Move{wizardId, world.Me[wizardId].Position, 0}
}

func MovetoFirstSnaffle(wizardId int, world *World) Action {
	if len(world.SnaffleIds) < 0 {
		return StopMoving(wizardId, world)
	}
	fmt.Fprintf(os.Stderr, "Wizard %d: MovetoFirstSnaffle\n", wizardId)
	return Move{wizardId, world.Entities[world.SnaffleIds[0]].Position, 150}
}

func EntitySpeed(e Entity) int {
	return int(math.Sqrt(float64(e.Vx*e.Vx + e.Vy*e.Vy)))
}

func MoveToEntity(snaffleId int, wizardId int, world *World) Action {
	snaffle := world.Entities[snaffleId]
	target := EndPos(world.Entities[snaffleId])
	if EntitySpeed(snaffle) < 200 {
		target = world.Entities[snaffleId].Position
	}
	if target.X < 0 {
		target.X = 0
	}
	if target.X > 16000 {
		target.X = 16000
	}

	return Move{wizardId, target, 150}
}

func DefendGoal(wizardId int, world *World) Action {
	fmt.Fprintf(os.Stderr, "Wizard %d: DefendGoal\n", wizardId)
	world.SnaffleIds.SortByDistTo(world.MyGoal, world)
	for i := wizardId / 2; i < len(world.SnaffleIds); i += 2 {
		t := world.SnaffleIds[i]
		nextPos := Position{
			world.Entities[t].X + world.Entities[t].Vx,
			world.Entities[t].Y + world.Entities[t].Vy,
		}
		if nextPos.X < 0 || nextPos.X > 16000 {
			fmt.Fprintf(os.Stderr, "Lost cause\n")
			continue
		}
		if AimAtGoal(world.Entities[t].Position, nextPos, world.MyGoal, 1500, world) {
			endX := world.Entities[t].X + 4*world.Entities[t].Vx
			if world.MP > 30 && endX < 0 || endX > 16000 {
				return Petrificus{t, wizardId}
			}
		} else {
			fmt.Fprintf(os.Stderr, "%d: Not aimed, %v, %v\n", t, world.Entities[t].Position, nextPos)
		}

		return PullOrMoveToSnaffle(t, wizardId, world)
	}
	return Move{wizardId, world.MyGoal, 150}
}

func PushRemoteSnaffle(wizardId int, world *World) Action {
	fmt.Fprintf(os.Stderr, "Wizard %d: PullRemoteSnaffle\n", wizardId)
	target := ClosestSnaffle(world.MyGoal, world)
	return Move{wizardId, world.Entities[target].Position, 150}
}

func PullOrMoveToSnaffle(snaffleId, wizardId int, world *World) Action {
	fmt.Fprintf(os.Stderr, "Wizard %d: PullOrMoveToSnaffle\n", wizardId)
	sPos := world.Entities[snaffleId].Position
	wPos := world.Entities[wizardId].Position
	if world.MP < 20 ||
		world.Me[wizardId].ACoolDown < 0 ||
		world.Entities[snaffleId].Vx < -500 ||
		world.Entities[snaffleId].Vx > 500 {
		return MoveToEntity(snaffleId, wizardId, world)
	}
	world.SnaffleIds.SortByDistTo(world.MyGoal, world)
	if Distance(world.Entities[world.SnaffleIds[len(world.SnaffleIds)/2]].Position, world.MyGoal) <
		Distance(world.Me[wizardId].Position, world.MyGoal) {
		return MoveToEntity(snaffleId, wizardId, world)
	}
	if len(world.SnaffleIds) == 1 ||
		snaffleId == world.SnaffleIds[0] ||
		snaffleId == world.SnaffleIds[1] {
		if world.MyGoal.X < 10 {
			if sPos.X < wPos.X && wPos.X-sPos.X < 7500 && wPos.X-sPos.X > 3000 {
				return Accio{wizardId, snaffleId}
			}
		} else {
			if sPos.X < wPos.X && sPos.X-wPos.X < 7500 && sPos.X-wPos.X > 3000 {
				return Accio{wizardId, snaffleId}
			}
		}
	}

	return MoveToEntity(snaffleId, wizardId, world)
}

func MoveToStrike(wizardId int, world *World) Action {
	fmt.Fprintf(os.Stderr, "Wizard %d: StrikeGoal\n", wizardId)
	world.SnaffleIds.SortByDistTo(world.OppGoal, world)
	for _, t := range world.SnaffleIds {
		targetPos := Position{
			world.Entities[t].X + 4*world.Entities[t].Vx,
			world.Entities[t].Y + 4*world.Entities[t].Vy,
		}
		if world.OppGoal.X > 10 {
			if targetPos.X > world.OppGoal.X {
				continue
			}
		} else {
			if targetPos.X < world.OppGoal.X {
				continue
			}
		}
		return PullOrMoveToSnaffle(t, wizardId, world)
	}
	return Move{wizardId, Position{8000, 3750}, 150}
}

func ThrowSnaffle(wizardId int, world *World) Action {
	snaffleId := -10
	for _, id := range world.SnaffleIds {
		if world.Entities[id].Position == world.Entities[wizardId].Position {
			snaffleId = id
		}
	}
	from := NextPos(world.Entities[wizardId], world)
	targets := []Position{
		world.OppGoal,
		Position{world.OppGoal.X, world.OppGoal.Y + 7500},
		Position{world.OppGoal.X, world.OppGoal.Y - 7500},
	}
TargetList:
	for _, target := range targets {
		e := Entity{from, Speed{}, 0.0, 150}
		vX := e.Vx + 500*(target.X-e.X)/Distance(e.Position, target)
		vY := e.Vy + 500*(target.Y-e.Y)/Distance(e.Position, target)
		target.Y = target.Y - world.Entities[wizardId].Vy*4
		e.Speed = Speed{vX, vY}
		for _, oId := range world.OpponentIds {
			if Colliding(e, world.Entities[oId]) {
				fmt.Fprintf(os.Stderr, "Collition: %d %v %v\n", oId, e, world.Entities[oId])
				continue TargetList
			}
		}
		for _, oId := range world.BludgerIds {
			if Colliding(e, world.Entities[oId]) {
				fmt.Fprintf(os.Stderr, "Collition: %d %v %v\n", oId, e, world.Entities[oId])
				continue TargetList
			}
		}
		return Throw{snaffleId, wizardId, target, 500}
	}
	return Throw{snaffleId, wizardId, Position{world.Entities[wizardId].X, world.Entities[wizardId].Y + world.Entities[wizardId].Vy}, 500}
}

func FindSnaffleTargets(p1, p2 Entity, world *World) (int, int) {
	world.SnaffleIds.SortByEndDistTo(NextPos(p1, world), world)
	p1C := world.SnaffleIds[0]
	world.SnaffleIds.SortByEndDistTo(NextPos(p2, world), world)
	p2C := world.SnaffleIds[0]
	world.SnaffleIds.SortByEndDistTo(world.MyGoal, world)
	p1Pos := EndPos(p1)
	p2Pos := EndPos(p2)
	i1, i2 := 0, 1
	if len(world.SnaffleIds) > 2 &&
		Distance(EndPos(world.Entities[world.SnaffleIds[i1]]),
			EndPos(world.Entities[world.SnaffleIds[i2]])) < 3000 {
		i2 = 2
	}
	p1s0 := Distance(p1Pos, EndPos(world.Entities[world.SnaffleIds[0]]))
	p1s1 := Distance(p1Pos, EndPos(world.Entities[world.SnaffleIds[1]]))
	p2s0 := Distance(p2Pos, EndPos(world.Entities[world.SnaffleIds[0]]))
	p2s1 := Distance(p2Pos, EndPos(world.Entities[world.SnaffleIds[1]]))
	if p1s0+p2s1 < p1s1+p2s0 {
		t1 := world.SnaffleIds[0]
		t2 := world.SnaffleIds[1]
		if Distance(p1Pos, EndPos(world.Entities[p1C]))*3 < p1s0 {
			t1 = p1C
		}
		if Distance(p2Pos, EndPos(world.Entities[p2C]))*3 < p2s1 {
			t2 = p2C
		}
		return t1, t2
	}
	t1 := world.SnaffleIds[1]
	t2 := world.SnaffleIds[0]
	if Distance(p1Pos, EndPos(world.Entities[p1C]))*3 < p1s1 {
		t1 = p1C
	}
	if Distance(p2Pos, EndPos(world.Entities[p2C]))*3 < p2s0 {
		t2 = p2C
	}
	return t1, t2
}

func DefaultStrategy(world *World) []Action {
	wi1 := world.MeIds[0]
	wi2 := world.MeIds[1]
	w1 := world.Me[wi1]
	w2 := world.Me[wi2]
	var t1, t2 int
	if len(world.SnaffleIds) < 2 {
		world.OpponentIds.SortByNextDistTo(EndPos(world.Entities[world.SnaffleIds[0]]), world)
		if Distance(w1.Position, world.Entities[world.SnaffleIds[0]].Position) <
			Distance(w2.Position, world.Entities[world.SnaffleIds[0]].Position) {
			t1, t2 = world.SnaffleIds[0], world.OpponentIds[0]
			return []Action{
				PullOrMoveToSnaffle(t1, wi1, world),
				MoveToEntity(t2, wi2, world),
			}
		} else {
			t1, t2 = world.OpponentIds[0], world.SnaffleIds[0]
			return []Action{
				MoveToEntity(t1, wi1, world),
				PullOrMoveToSnaffle(t2, wi2, world),
			}
		}
	} else {
		t1, t2 = FindSnaffleTargets(w1.Entity, w2.Entity, world)
		return []Action{
			PullOrMoveToSnaffle(t1, wi1, world),
			PullOrMoveToSnaffle(t2, wi2, world),
		}
	}
}

func ThrowIfHolding(s Strategy) Strategy {
	return func(world *World) []Action {
		actions := s(world)
		wi1 := world.MeIds[0]
		wi2 := world.MeIds[1]
		w1 := world.Me[wi1]
		w2 := world.Me[wi2]
		if w1.State == 1 {
			actions[0] = ThrowSnaffle(wi1, world)
		}
		if w2.State == 1 {
			actions[1] = ThrowSnaffle(wi2, world)
		}
		return actions
	}
}

func ChargeIfAccio(s Strategy) Strategy {
	return func(world *World) []Action {
		actions := s(world)
		wi1 := world.MeIds[0]
		wi2 := world.MeIds[1]
		w1 := world.Me[wi1]
		w2 := world.Me[wi2]
		if w1.ACoolDown > 0 && Distance(w1.Position, world.Entities[w1.AEntity].Position) < 3000 {
			fmt.Fprintf(os.Stderr, "W1 %d dist: %d\n", w1.AEntity, Distance(w1.Position, world.Entities[w1.AEntity].Position))
			actions[0] = Move{wi1, world.OppGoal, 150}
		}
		if w2.ACoolDown > 0 && Distance(w2.Position, world.Entities[w2.AEntity].Position) < 3000 {
			actions[1] = Move{wi2, world.OppGoal, 150}
		}
		return actions
	}
}

func FlipendoIfGoal(s Strategy) Strategy {
	return func(world *World) []Action {
		actions := s(world)
		fmt.Fprintf(os.Stderr, "MP: %d\n", world.MP)
		mp := world.MP
		if mp < 21 {
			return actions
		}
		OppTop := Position{world.OppGoal.X, world.OppGoal.Y - 1800}
		OppBot := Position{world.OppGoal.X, world.OppGoal.Y + 1800}
	Outer:
		for _, sId := range world.SnaffleIds {
			if mp < 21 {
				break
			}
			snafflePos := world.Entities[sId].Position
			snaffleNextPos := NextPos(world.Entities[sId], world)
			for _, wId := range world.MeIds {
				if Distance(snafflePos, world.Entities[wId].Position) < 551 {
					continue Outer
				}
			}
			for _, oId := range world.OpponentIds {
				if Distance(snafflePos, world.Entities[oId].Position) < 551 ||
					PointInTriangle(NextPos(world.Entities[oId], world), snaffleNextPos, AddY(-500, OppTop), AddY(500, OppBot)) {
					continue Outer
				}
			}
			for i, wId := range world.MeIds {
				if world.Me[wId].FCoolDown > 0 {
					continue
				}
				wPos := NextPos(world.Entities[wId], world)
				dist := Distance(snaffleNextPos, wPos)
				if dist < 1000 || dist > 4000 {
					continue
				}
				if PointInTriangle(snaffleNextPos, wPos, OppTop, OppBot) {
					actions[i] = Flipendo{wId, sId}
					mp -= 20
					continue Outer
				}
			}
		}
		return actions
	}
}

func SamePos(e1, e2 Entity) bool {
	return e1.X == e2.X && e1.Y == e2.Y
}

func PetrificusIfGoal(s Strategy) Strategy {
	return func(world *World) []Action {
		actions := s(world)
	Outer:
		for _, sId := range world.SnaffleIds {
			for _, pL := range world.PetriLast {
				if pL == sId {
					continue Outer
				}
			}
			if Distance(NextPos(world.Entities[sId], world), NextPos(world.Entities[world.MeIds[0]], world)) < 400 ||
				Distance(NextPos(world.Entities[sId], world), NextPos(world.Entities[world.MeIds[0]], world)) < 400 {
				continue
			}
			fmt.Fprintf(os.Stderr, "wsDist: %d %d %d\n", world.MeIds[0], sId, Distance(NextPos(world.Entities[sId], world), NextPos(world.Entities[world.MeIds[0]], world)))
			fmt.Fprintf(os.Stderr, "wsDist: %d %d %d\n", world.MeIds[1], sId, Distance(NextPos(world.Entities[sId], world), NextPos(world.Entities[world.MeIds[1]], world)))
			nextPos := NextPos(world.Entities[sId], world)
			if nextPos.X < 0 || nextPos.X > 16000 {
				continue
			}
			if AimAtGoal(world.Entities[sId].Position, nextPos, world.MyGoal, 1800, world) {
				endX := world.Entities[sId].X + 4*world.Entities[sId].Vx
				if world.MP > 10 && (endX < 0 || endX > 16000) {
					actions[0] = Petrificus{sId, world.MeIds[0]}
					if world.MP > 30 {
						actions[1] = Accio{world.MeIds[1], sId}
					}
				}
			}
		}
		return actions
	}
}

func readState(w *World) *World {
	w.MP += 1
	// entities: number of entities still in game
	var entities int
	fmt.Scan(&entities)
	w.SnaffleIds = []int{}
	w.BludgerIds = []int{}
	w.MeIds = []int{}
	w.OpponentIds = []int{}

	for i := 0; i < entities; i++ {
		var entityId int
		var entityType string
		var x, y, vx, vy, state int
		fmt.Scan(&entityId, &entityType, &x, &y, &vx, &vy, &state)
		fmt.Fprintf(os.Stderr, "%d %s %d %d %d %d %d\n", entityId, entityType, x, y, vx, vy, state)
		e := Entity{
			Position{x, y},
			Speed{vx, vy},
			0.0,
			0,
		}
		w.Entities[entityId] = e
		switch entityType {
		case "WIZARD":
			w.MeIds = append(w.MeIds, entityId)
			w.Me[entityId].Entity = e
			w.Me[entityId].State = state
			w.Entities[entityId].Friction = 0.75
			w.Entities[entityId].Radius = 400
		case "OPPONENT_WIZARD":
			w.OpponentIds = append(w.OpponentIds, entityId)
			w.Opponent[entityId].Entity = e
			w.Opponent[entityId].State = state
			w.Entities[entityId].Friction = 0.75
			w.Entities[entityId].Radius = 400
		case "SNAFFLE":
			w.SnaffleIds = append(w.SnaffleIds, entityId)
			w.Entities[entityId].Radius = 150
			w.Entities[entityId].Friction = 0.75
		case "BLUDGER":
			w.BludgerIds = append(w.BludgerIds, entityId)
			w.Entities[entityId].Radius = 200
			w.Entities[entityId].Friction = 0.9
		}
	}
	return w
}

func main() {
	// myTeamId: if 0 you need to score on the right of the map, if 1 you need to score on the left
	var myTeamId int
	fmt.Scan(&myTeamId)
	world := &World{}
	if myTeamId == 0 {
		world.MyGoal = Position{0, 3750}
		world.OppGoal = Position{16000, 3750}
	} else {
		world.MyGoal = Position{16000, 3750}
		world.OppGoal = Position{0, 3750}
	}

	world = readState(world)
	strategy := FlipendoIfGoal(PetrificusIfGoal(ThrowIfHolding(ChargeIfAccio(DefaultStrategy))))
	for {
		for i := 0; i < len(world.MeIds); i++ {
			wizId := world.MeIds[i]
			world.Me[wizId].OCoolDown -= 1
			world.Me[wizId].ACoolDown -= 1
			world.Me[wizId].FCoolDown -= 1
		}
		actions := strategy(world)
		world.PetriLast = EntityList{}
		for _, action := range actions {
			world = action.Apply(world)
		}
		world = readState(world)
	}
}
