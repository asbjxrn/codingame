package main

import (
	"fmt"
	"os"
)

func debug(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

type point struct {
	x, y int
}

func main() {
	// surfaceN: the number of points used to draw the surface of Mars.
	var surfaceN int
	fmt.Scan(&surfaceN)
	surface := make([]point, surfaceN)
	var zone1, zone2 point

	for i := 0; i < surfaceN; i++ {
		// landX: X coordinate of a surface point. (0 to 6999)
		// landY: Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
		var landX, landY int
		fmt.Scan(&landX, &landY)
		surface[i] = point{landX, landY}
		if i != 0 && surface[i].y == surface[i-1].y {
			zone1 = surface[i-1]
			zone2 = surface[i]
		}
	}
	for {
		// hSpeed: the horizontal speed (in m/s), can be negative.
		// vSpeed: the vertical speed (in m/s), can be negative.
		// fuel: the quantity of remaining fuel in liters.
		// rotate: the rotation angle in degrees (-90 to 90).
		// power: the thrust power (0 to 4).
		var X, Y, hSpeed, vSpeed, fuel, rotate, power int
		fmt.Scan(&X, &Y, &hSpeed, &vSpeed, &fuel, &rotate, &power)
		debug("z1: %v, z2: %v\n", zone1, zone2)
		debug("X: %dY: %d, hSpeed: %d, vSpeed: %d, fuel: %d, rotate: %d, power: %d",
			X, Y, hSpeed, vSpeed, fuel, rotate, power)
		var r, p int
		switch {
		case Y < zone1.y+100:
			debug("\ncase1\n")
			r = 0
		case hSpeed < -60:
			r = -70
		case hSpeed > 60:
			r = 70
		case X > zone1.x && X < zone2.x:
			debug("\ncase2\n")
			if hSpeed < -10 {
				r = -30
			} else if hSpeed > 10 {
				r = 30
			}
		case X < zone1.x && hSpeed < 40 || hSpeed < -40:
			debug("\ncase3\n")
			r = -35
		case X > zone2.x && hSpeed > -40 || hSpeed > 40:
			debug("\ncase4\n")
			r = 35
		default:
			debug("\nfallthrough")
		}
		if vSpeed < -35 {
			p = 4
		} else {
			p = 3
		}

		fmt.Printf("%d %d\n", r, p)
	}
}
