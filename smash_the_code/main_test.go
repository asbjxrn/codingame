package main

import "testing"

func TestPartition(t *testing.T) {
	Setup()
	s := Stack{
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'},
	}
	g := Partition(s)
	if len(g) != 0 {
		t.Error("Empty board returned some partitions")
	}
	cleanSeen()
	s = Stack{
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '1', '1'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '2', '2'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '4', '2'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '4', '4'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '3', '3'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '3'},
	}
	g = Partition(s)
	// No starting point to search, seen is all zeroes
	if len(g) != 0 {
		t.Errorf("Wrong Number of partitions, got %d, expected 2", len(g))
	}
	cleanSeen()
	seen[2][11] = 1
	g = Partition(s)
	if len(g) != 1 {
		t.Errorf("Wrong number of partitions, got %d, expected 1", len(g))
	}
	if len(g[0]) != 3 {
		t.Errorf("Wrong number of elements in group, got %d, expected 3", len(g[0]))
	}
}
