package main

import (
	"fmt"
	"math"
	"os"
)

func debug(s string) {
	fmt.Fprintln(os.Stderr, s)
}

type pos struct {
	X, Y, Id int
}

func abs(i int) int {
	if i < 0 {
		i = -i
	}
	return i
}

func (p pos) dist(t pos) float64 {
	x := float64(p.X - t.X)
	y := float64(p.Y - t.Y)
	return math.Sqrt(x*x + y*y)
}

func readInput() (ash pos, humans, zombies []pos) {
	var x, y int
	fmt.Scan(&x, &y)
	ash = pos{x, y, 0}
	humans = []pos{}
	var humanCount int
	fmt.Scan(&humanCount)
	for i := 0; i < humanCount; i++ {
		var humanId, humanX, humanY int
		fmt.Scan(&humanId, &humanX, &humanY)
		humans = append(humans, pos{humanX, humanY, humanId})
	}
	var zombieCount int
	zombies = []pos{}
	fmt.Scan(&zombieCount)
	for i := 0; i < zombieCount; i++ {
		var zombieId, zombieX, zombieY, zombieXNext, zombieYNext int
		fmt.Scan(&zombieId, &zombieX, &zombieY, &zombieXNext, &zombieYNext)
		zombies = append(zombies, pos{zombieXNext, zombieYNext, zombieId})
	}
	return ash, humans, zombies
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func limitx(x int) int {
	return min(max(0, x), 16000)
}

func limity(y int) int {
	return min(max(0, y), 9000)
}

func flankZombies(ash pos, zombies []pos) (x, y int) {
	var z1, z2 pos
	maxrange := float64(0)
	for i := range zombies {
		for j := range zombies {
			if zombies[i].dist(zombies[j]) >= maxrange {
				z1 = pos{zombies[i].X, zombies[i].Y, zombies[i].Id}
				z2 = pos{zombies[j].X, zombies[j].Y, zombies[j].Id}
				maxrange = z1.dist(z2)
			}
		}
	}
	debug(fmt.Sprintf("Z1: %v\nZ2: %v", z1, z2))
	dx := abs(z1.X-z2.X) / 2
	dy := abs(z1.Y-z2.Y) / 2
	mid := pos{(z1.X + z2.X) / 2, (z1.Y + z2.Y) / 2, 0}
	if z1.dist(z2) < 4000 && ash.dist(mid) < 2300 {
		debug(fmt.Sprintf("mid: %v \n", mid))
		return mid.X, mid.Y
	}
	var p1, p2 pos
	if (z1.X < z2.X && z1.Y < z2.Y) || (z1.X > z2.X && z1.Y > z2.Y) {
		p1 = pos{limitx(mid.X - dy), limity(mid.Y + dx), 0}
		p2 = pos{limitx(mid.X + dy), limity(mid.Y - dx), 0}
	} else {
		p1 = pos{limitx(mid.X - dy), limity(mid.Y - dx), 0}
		p2 = pos{limitx(mid.X + dy), limity(mid.Y + dx), 0}
	}
	debug(fmt.Sprintf("dx: %d\ndy: %d\nmid: %v\nP1: %v (%v) \nP2: %v (%v) \n", dx, dy, mid, p1, ash.dist(p1), p2, ash.dist(p2)))
	if ash.dist(p1) < ash.dist(p2) {
		return p1.X, p1.Y
	}
	return p2.X, p2.Y
}

func main() {
	lfd := map[int]bool{}
	for {
		ash, humans, zombies := readInput()
		var dx, dy int
		ashdist := float64(40000)
		closestHuman := humans[0]
		closestToAll := true
	outer:
		for _, v := range humans {
			if lfd[v.Id] {
				debug(fmt.Sprintf("skipping: %d", v.Id))
				continue
			}
			ashClosest := true
			for _, z := range zombies {
				if ash.dist(z) < z.dist(v) {
					continue
				}
				// Ignore humans we can't save (zomie will reach before I do.)
				// (Assuming opposide sides, distracting zome might improve.)
				// if ash.dist(v) > 3000 && (ash.dist(v)/1000)-2 > z.dist(v)/300 {
				if (ash.dist(v)-3000)/1000 > z.dist(v)/300 {
					debug(fmt.Sprintf("Ignoring human: %d (z: %d)", v.Id, z.Id))
					closestToAll = false
					lfd[v.Id] = true
					continue outer
				}
				if ash.dist(v)-500 > z.dist(v) {
					closestToAll = false
					ashClosest = false
				}
			}
			if ashClosest {
				debug(fmt.Sprintf("ashClosest to: %d", v.Id))
				continue
			}
			closestToAll = false
			if ashdist < ash.dist(v) {
				debug(fmt.Sprintf("ash closer to another human than: %d", v.Id))
				continue
			}
			ashdist = ash.dist(v)
			closestHuman = v
		}
		switch {
		case closestToAll && len(zombies) > 1:
			debug("flanking")
			dx, dy = flankZombies(ash, zombies)
		case closestHuman.X == 30000 && closestHuman.Y == 30000 && len(zombies) > 1:
			debug("no close humans")
			dx, dy = flankZombies(ash, zombies)
		case dx == 0 && dy == 0:
			cz := zombies[0]
			for _, z := range zombies {
				if closestHuman.dist(z) < closestHuman.dist(cz) {
					cz = z
				}
			}
			dx = (cz.X + closestHuman.X) / 2
			dy = (cz.Y + closestHuman.Y) / 2
		default:
		}
		fmt.Printf("%d %d\n", dx, dy) // Your destination coordinates

	}
}
