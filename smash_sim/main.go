package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"os/exec"

	"github.com/pkg/profile"
)

const (
	Blocker = '0'
	Empty   = '.'
	Marked  = 'x'
)

var Debug bool = false

type Stack [6][12]rune

type State struct {
	P1, P2     Player
	Blocks     [8][2]rune
	P2Stack    *Stack
	P1Stack    *Stack
	P1Score    int
	P2Score    int
	P1Nuisance float64
	P2Nuisance float64
}

type Move struct {
	col, rotation  int
	colorA, colorB rune
}

type Point struct {
	X, Y int
}

type Player struct {
	ID       int
	Score    int
	Nuisance float64
	Cmd      *exec.Cmd
	Stdin    io.WriteCloser
	Stdout   io.ReadCloser
}

var NeighborMap [6][12][]Point

func printStack(o io.Writer, s *Stack) {
	for i := 0; i < 12; i++ {
		for j := 0; j < 6; j++ {
			fmt.Fprintf(o, "%c", s[j][i])
		}
		fmt.Fprintf(o, "\n")
	}
}

func debug(s string) {
	//fmt.Fprintf(os.Stderr, "%s", s)
}

func input(s string) {
	fmt.Fprintf(os.Stderr, "input: %s\n", s)
}

func output(s string) {
	debug(fmt.Sprintf("output: %s\n", s))
	fmt.Print(s)
}

func Setup(s *State) (p1, p2 Player) {
	for i := 0; i < 8; i++ {
		s.Blocks[i][0] = randCol()
		s.Blocks[i][1] = randCol()
	}
	s.P1Stack = &Stack{}
	s.P2Stack = &Stack{}
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			s.P1Stack[i][j] = Empty
			s.P2Stack[i][j] = Empty
		}
	}
	p1cmd := exec.Command("./player1/player1")
	p1 = Player{
		ID:  1,
		Cmd: p1cmd,
	}
	p1.Stdin, _ = p1cmd.StdinPipe()
	p1.Stdout, _ = p1cmd.StdoutPipe()
	p1.Cmd.Start()
	p2cmd := exec.Command("./player2/player2")
	p2 = Player{
		ID:  2,
		Cmd: p2cmd,
	}
	p2.Stdin, _ = p2cmd.StdinPipe()
	p2.Stdout, _ = p2cmd.StdoutPipe()
	p2.Cmd.Start()
	return p1, p2
}

func findGroup(s Stack, p Point, seen *[6][12]byte) []Point {
	set := []Point{p}
	seen[p.X][p.Y] = 2
	color := s[p.X][p.Y]
	for p := 0; p < len(set); p++ {
		for _, n := range NeighborMap[set[p].X][set[p].Y] {
			if seen[n.X][n.Y] < 2 && s[n.X][n.Y] == color {
				set = append(set, n)
				seen[n.X][n.Y] = 2
			}
		}
	}
	return set
}

func Partition(s Stack) [][]Point {
	seen := [6][12]byte{}
	sets := [][]Point{}
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			if s[i][j] == Empty || s[i][j] == Blocker {
				continue
			}
			this := Point{i, j}
			set := findGroup(s, this, &seen)
			if len(set) > 3 {
				sets = append(sets, set)
			}
		}
	}
	return sets
}

func Compress(s *Stack) {
	for i := 0; i < 6; i++ {
		down := 0
		for j := 11; j >= 0; j-- {
			if s[i][j] == Empty {
				down++
			} else {
				s[i][j+down] = s[i][j]
			}
		}
		for j := 0; j < down; j++ {
			s[i][j] = Empty
		}
	}
}

func Collapse(s *Stack) int {
	score := 0
	var deleted, chain, colors, groupbonus int
	var seenColors [5]rune
	//	fmt.Printf("B Collaps: %v\n", s)
	for {
		deleted = 0
		groups := Partition(*s)
		for _, group := range groups {
			groupSize := len(group)
			if groupSize > 3 {
				deleted += groupSize
				groupbonus = groupSize - 4
				for _, p := range group {
					seenColors[s[p.X][p.Y]-'1'] = 1
					s[p.X][p.Y] = Empty
					for _, n := range NeighborMap[p.X][p.Y] {
						if s[n.X][n.Y] == Blocker {
							s[n.X][n.Y] = Empty
						}
					}
				}
			}
		}
		Compress(s)
		for i := 0; i < 5; i++ {
			if seenColors[i] != 0 {
				colors++
			}
			seenColors[i] = 0
		}
		if colors > 1 {
			colors = 1 << uint(colors-1)
		} else {
			colors = 0
		}
		bonus := chain + colors + groupbonus
		colors = 0
		if bonus < 1 {
			bonus = 1
		}
		score += (10 * deleted) * bonus
		if chain > 0 {
			chain *= 2
		} else {
			chain = 8
		}
		if deleted == 0 {
			return score
		}
		deleted = 0
	}
}

func addBricks(s *Stack, m Move) error {
	if (m.col == 0 && m.rotation == 2) || (m.col == 5 && m.rotation == 0) {
		fmt.Errorf("Invalid move: %v\n", m)
	}
	if (m.rotation == 0 && s[m.col+1][0] != Empty) ||
		(m.rotation == 2 && s[m.col-1][0] != Empty) ||
		((m.rotation == 1 || m.rotation == 3) && s[m.col][1] != Empty) {
		return fmt.Errorf("No room for new bricks")
	}
	switch m.rotation {
	case 0:
		s[m.col][0] = m.colorA
		s[m.col+1][0] = m.colorB
	case 1:
		s[m.col][1] = m.colorA
		s[m.col][0] = m.colorB
	case 2:
		s[m.col][0] = m.colorA
		s[m.col-1][0] = m.colorB
	case 3:
		s[m.col][0] = m.colorA
		s[m.col][1] = m.colorB
	default:
		log.Printf("Incorrect move: %d\n", m.rotation)
	}
	Compress(s)
	return nil
}

func makeMove(s *Stack, m Move) (int, error) {
	err := addBricks(s, m)
	if err != nil {
		return 0, err
	}
	return Collapse(s), nil
}

func AddRows(s *Stack, r int) {
	if r > 12 {
		r = 12
	}
	for i := 0; i < 6; i++ {
		for j := 0; j < r; j++ {
			if s[i][j] == Empty {
				s[i][j] = Blocker
			}
		}
	}
	Compress(s)
}

func randCol() rune {
	return rune(rand.Intn(5)) + '1'
}

func printState(s *State, p Player) {
	for i := 0; i < 8; i++ {
		fmt.Fprintf(p.Stdin, "%c %c\n", s.Blocks[i][0], s.Blocks[i][1])
	}
	if p.ID == 1 {
		printStack(p.Stdin, s.P1Stack)
		printStack(p.Stdin, s.P2Stack)
	} else {
		printStack(p.Stdin, s.P2Stack)
		printStack(p.Stdin, s.P1Stack)
	}
}

func match(reverse bool) int {
	s := State{}
	p1, p2 := Setup(&s)
	if reverse {
		p1, p2 = p2, p1
		p1.ID = 1
		p2.ID = 2
	}
	var p1Col, p1Rot int
	var p2Col, p2Rot int
	winner := 0
	for {
		printState(&s, p1)
		n, err := fmt.Fscanf(p1.Stdout, "%d %d\n", &p1Col, &p1Rot)
		if err != nil || n != 2 {
			fmt.Print(err)
			winner = 2
			break
		}
		m1, err := makeMove(s.P1Stack, Move{p1Col, p1Rot, s.Blocks[0][0], s.Blocks[0][1]})
		if err != nil {
			//		log.Printf("P1 error: %v", err)
			winner = 2
			break
		}
		printState(&s, p2)
		n, err = fmt.Fscanf(p2.Stdout, "%d %d\n", &p2Col, &p2Rot)
		if err != nil || n != 2 {
			fmt.Print(err)
			winner = 1
			break
		}
		m2, err := makeMove(s.P2Stack, Move{p2Col, p2Rot, s.Blocks[0][0], s.Blocks[0][1]})
		if err != nil {
			//		log.Printf("P2 error: %v", err)
			winner = 1
			break
		}
		p1.Score += m1
		p1.Nuisance += float64(m1) / 70.0
		if p1.Nuisance > 6 {
			rows := int(p1.Nuisance / 6)
			p1.Nuisance -= float64(rows) * 6.0
			AddRows(s.P2Stack, rows)
		}
		p2.Score += m2
		p2.Nuisance += float64(m2) / 70.0
		if p2.Nuisance > 6 {
			rows := int(p2.Nuisance / 6)
			p2.Nuisance -= float64(rows) * 6.0
			AddRows(s.P1Stack, rows)
		}
		for i := 0; i < 7; i++ {
			s.Blocks[i][0] = s.Blocks[i+1][0]
			s.Blocks[i][1] = s.Blocks[i+1][1]
		}
		s.Blocks[7][0] = randCol()
		s.Blocks[7][1] = randCol()
	}
	if Debug {
		fmt.Printf("Winner: %d\n", winner)
		fmt.Printf("Moves: %d %d\n       %d %d\n", p1Col, p1Rot, p2Col, p2Rot)
		fmt.Printf("Score: %6d   %6d\n", p1.Score, p2.Score)
		fmt.Printf("Nuisance: %6f   %6f\n", p1.Nuisance, p2.Nuisance)
		for i := 0; i < 8; i++ {
			fmt.Printf("%c %c\n", s.Blocks[i][0], s.Blocks[i][0])
		}
		printStack(os.Stderr, s.P1Stack)
		printStack(os.Stderr, s.P2Stack)
		fmt.Println()
	}
	return winner
}

func run() {
	wins := [20]int{}
	sum := [2]int{}
	for i := 0; i < 20; i++ {
		rand.Seed(int64(i))
		wins[i] = match(false) - 1
		sum[wins[i]]++
		//		rand.Seed(int64(i))
		//	wins[i+10] = 1 - (match(true) - 1)
	}
	//	fmt.Println(wins)
	fmt.Printf("%d %d\n", sum[0], sum[1])
}

func main() {
	mode := flag.String("profile.mode", "", "enable profiling mode, one of [cpu, mem, block]")
	flag.Parse()
	switch *mode {
	case "cpu":
		defer profile.Start(profile.CPUProfile).Stop()
	case "mem":
		defer profile.Start(profile.MemProfile).Stop()
	case "block":
		defer profile.Start(profile.BlockProfile).Stop()
	default:
		// do nothing
	}
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			set := []Point{}
			if i > 0 {
				set = append(set, Point{i - 1, j})
			}
			if i < 5 {
				set = append(set, Point{i + 1, j})
			}
			if j > 0 {
				set = append(set, Point{i, j - 1})
			}
			if j < 11 {
				set = append(set, Point{i, j + 1})
			}
			NeighborMap[i][j] = set
		}
	}
	run()
}
