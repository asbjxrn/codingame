package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"
)

const (
	GroupFactor   = 4
	ColorFactor   = 2
	HeightFactor  = 2
	BlockerFactor = 2
	AntiDropGS    = 3
	AntiDropValue = 200
	DropTrigger   = 5
	Blocker       = '0'
	Empty         = '.'
	Marked        = 'x'
)

type Stack [6][12]rune

type State struct {
	Blocks     [][2]rune
	UsedBlocs  int
	MyStack    Stack
	MyBlocks   int
	HisStack   Stack
	HisBlocks  int
	Score      int
	GroupScore int
	NewBlocs   [2][2]int
	Deleted    int
}

type Move struct {
	Column     int
	Rotation   int
	ColorA     rune
	ColorB     rune
	Score      int
	OwnScore   int
	ChildScore int
	GroupScore int
	HeightPen  int
	ColorPen   int
	StackBonus int
	Dropping   int
	Deleted    int
	Movelist   string
}

type Point struct {
	X, Y int
}

var NeighborMap [6][12][]Point

func printState(s State) {
	for i := 0; i < 12; i++ {
		for j := 0; j < 6; j++ {
			fmt.Fprintf(os.Stderr, "%c", s.MyStack[j][i])
		}
		fmt.Fprintf(os.Stderr, "\n")
	}
}

func debug(s string) {
	//fmt.Fprintf(os.Stderr, "%s", s)
}

func input(s string) {
	//fmt.Fprintf(os.Stderr, "input: %s\n", s)
}

func output(s string) {
	debug(fmt.Sprintf("output: %s\n", s))
	fmt.Print(s)
}

func Setup() {
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			set := []Point{}
			if i > 0 {
				set = append(set, Point{i - 1, j})
			}
			if i < 5 {
				set = append(set, Point{i + 1, j})
			}
			if j > 0 {
				set = append(set, Point{i, j - 1})
			}
			if j < 11 {
				set = append(set, Point{i, j + 1})
			}
			NeighborMap[i][j] = set
		}
	}
}

var seen [6][12]byte

// 0 == Not inspected
// 1 == Search may be started
// 2 == Already part of group
func cleanSeen() {
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			seen[i][j] = 1
		}
	}
}

func findGroup(s *Stack, p Point) []Point {
	set := []Point{p}
	seen[p.X][p.Y] = 2
	color := s[p.X][p.Y]
	for i := 0; i < len(set); i++ {
		for _, n := range NeighborMap[set[i].X][set[i].Y] {
			if seen[n.X][n.Y] < 2 && s[n.X][n.Y] == color {
				set = append(set, n)
				seen[n.X][n.Y] = 2
			}
		}
	}
	return set
}

func Partition(s *Stack) [][]Point {
	sets := [][]Point{}
	for i := 0; i < 6; i++ {
		for j := 0; j < 12; j++ {
			if seen[i][j] != 1 {
				continue
			}
			if s[i][j] == Empty || s[i][j] == Blocker {
				continue
			}
			this := Point{i, j}
			set := findGroup(s, this)
			if len(set) > 1 {
				sets = append(sets, set)
			}
		}
	}
	return sets
}

func printStack(s *State) {
	return
	for j := 0; j < 12; j++ {
		for i := 0; i < 6; i++ {
			//			debug(fmt.Sprintf("%c", s.MyStack[i][j]))
		}
		debug("\n")
	}
}
func Compress(s *Stack) {
	for i := 0; i < 6; i++ {
		down := 0
		for j := 11; j >= 0; j-- {
			if s[i][j] == Empty {
				down++
			} else {
				s[i][j+down] = s[i][j]
			}
		}
		for j := 0; j < down; j++ {
			s[i][j] = Empty
		}
	}
}

func Collapse(s *State) {
	cleanSeen()
	x0, y0 := s.NewBlocs[0][0], s.NewBlocs[0][1]
	x1, y1 := s.NewBlocs[1][0], s.NewBlocs[1][1]
	seen[x0][y0] = 1
	seen[x1][y1] = 1
	if s.MyStack[x0][y0] == Empty || s.MyStack[x1][y1] == Empty {
		log.Fatalf("BOO! %d %d %d %d\n", x0, y0, x1, y1)
	}

	var deleted, chain, colors, groupbonus int
	var seenColors [5]rune
	for {
		deleted = 0
		groups := Partition(&s.MyStack)
		printStack(s)
		for _, group := range groups {
			groupSize := len(group)
			if groupSize > 3 {
				deleted += groupSize
				groupbonus = groupSize - 4
				for _, p := range group {
					seenColors[s.MyStack[p.X][p.Y]-'1'] = 1
					s.MyStack[p.X][p.Y] = Marked
					for _, n := range NeighborMap[p.X][p.Y] {
						if s.MyStack[n.X][n.Y] == Blocker {
							s.MyStack[n.X][n.Y] = Marked
						}
					}
				}
			} else if groupSize > 1 {
				s.GroupScore += groupSize * groupSize
			}
		}
		cleanSeen()
		s.MyBlocks = 0
		for i := 0; i < 6; i++ {
			down := 0
			for j := 11; j >= 0; j-- {
				if s.MyStack[i][j] == Marked {
					s.MyStack[i][down] = Empty
					down++
				} else {
					s.MyStack[i][j+down] = s.MyStack[i][j]
					if s.MyStack[i][j] != Empty {
						s.MyBlocks++
						if down > 0 {
							seen[i][j+down] = 1
						}
					}
				}
			}
		}
		for i := 0; i < 5; i++ {
			if seenColors[i] != 0 {
				colors++
			}
			seenColors[i] = 0
		}
		if colors > 1 {
			colors = 1 << uint(colors-1)
		} else {
			colors = 0
		}
		bonus := chain + colors + groupbonus
		colors = 0
		if bonus < 1 {
			bonus = 1
		}
		s.Score += (10 * deleted) * bonus
		if chain > 0 {
			chain *= 2
		} else {
			chain = 8
		}
		s.Deleted += deleted
		if deleted == 0 {
			break
		}
		deleted = 0
	}
}

func height(s *Stack, col int) int {
	for j := 0; j < 12; j++ {
		if s[col][j] != Empty {
			return 12 - j
		}
	}
	return 0
}

func addBricks(s State, m Move) (State, error) {
	s.GroupScore = 0
	var h1, h2, c1, c2 int
	c1 = m.Column
	h1 = 11 - height(&s.MyStack, c1)
	switch m.Rotation {
	case 0:
		c2 = c1 + 1
		h2 = 11 - height(&s.MyStack, c2)
	case 1:
		c2 = c1
		h2 = h1 - 1
	case 2:
		c2 = c1 - 1
		h2 = 11 - height(&s.MyStack, c2)
	case 3:
		c2 = c1
		h2 = h1
		h1 = h2 - 1
	}
	if h2 < 0 || h1 < 0 {
		return s, fmt.Errorf("No room for block.")
	}
	s.MyStack[c1][h1] = m.ColorB
	s.MyStack[c2][h2] = m.ColorA
	s.NewBlocs[0][0] = c1
	s.NewBlocs[0][1] = h1
	s.NewBlocs[1][0] = c2
	s.NewBlocs[1][1] = h2

	s.MyBlocks += 2
	return s, nil
}

func makeMove(s State, m Move) (State, error) {
	ns, err := addBricks(s, m)
	if err != nil {
		return ns, err
	}
	Collapse(&ns)
	return ns, nil
}

var heightPenalties [6][13]int = [6][13]int{
	{0, 2, 3, 10, 20, 20, 30, 30, 60, 70, 80, 80, 90},
	{0, 2, 3, 5, 10, 20, 20, 30, 30, 70, 80, 80, 90},
	{0, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 20, 30},
	{0, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 20, 30},
	{0, 2, 3, 5, 10, 20, 20, 30, 30, 70, 80, 80, 90},
	{0, 2, 3, 10, 20, 20, 30, 30, 60, 70, 80, 80, 90},
}

func findMove(s State, depth int, nuisance float64, oppMinHeight, oppDrops int) Move {
	var best Move
	bestScore := -10000
	colorA := s.Blocks[depth][0]
	colorB := s.Blocks[depth][1]
	for r := 0; r < 4; r += 1 {
		start, end := 0, 6
		if r == 0 {
			end = 5
		}
		if r == 2 {
			start = 1
		}
		for i := start; i < end; i++ {
			movelist := ""
			move := Move{
				Column:   i,
				Rotation: r,
				ColorA:   colorA,
				ColorB:   colorB,
			}
			state, err := makeMove(s, move)
			if err != nil {
				debug(fmt.Sprintf("Makemove failed: %v", err))
				continue
			}
			move.Deleted = state.Deleted
			move.GroupScore = state.GroupScore
			move.OwnScore = state.Score
			thisNuisance := nuisance + float64(state.Score)/70
			for thisNuisance >= 6.0 {
				move.Dropping++
				thisNuisance -= 6.0
			}
			score := state.Score + state.GroupScore
			colorColPen := 0
			for i := 0; i < 6; i++ {
				for j := 0; j < 12; j++ {
					color := state.MyStack[i][j]
					if color == Blocker {
						colorColPen += BlockerFactor
					} else if color != Empty {
						c := int(color-'0') - i
						if c < 0 {
							c = -c
						}
						colorColPen += c * ColorFactor
					}
				}
			}
			heightPen := 0
			for c := 0; c < 6; c++ {
				heightPen += heightPenalties[c][height(&state.MyStack, c)]
			}
			stackBonus := 0
			for i := 0; i < 6; i++ {
				var col rune
				for j := 11; j >= 0; j-- {
					if state.MyStack[i][j] == Empty {
						break
					}
					if col == state.MyStack[i][j] {
						stackBonus += 5
					}
					col = state.MyStack[i][j]
				}
			}
			score = score - HeightFactor*heightPen - colorColPen + stackBonus
			//score = score - colorColPen + stackBonus
			move.ColorPen = colorColPen
			move.StackBonus = stackBonus
			move.HeightPen = heightPen
			if move.Deleted > 0 && move.Dropping < AntiDropGS && heightPen < 150 {
				score = -AntiDropValue
			}
			if move.Dropping > DropTrigger-int(oppMinHeight)/3 || (move.Dropping > 0 && oppDrops > 2) {
				return move
			}
			if depth < 2 {
				bestChild := findMove(state, depth+1, thisNuisance, oppMinHeight, oppDrops)
				score += bestChild.Score
				move.ChildScore = bestChild.Score
				movelist = bestChild.Movelist
			}
			//			fmt.Fprintf(os.Stderr, "findMove Score: %v\n", score)
			if score > bestScore {
				best = move
				bestScore = score
				best.Score = score
				best.Movelist = fmt.Sprintf("%d:%d %s", move.Column, move.Rotation, movelist)
			}
		}
	}
	return best
}

func run() {
	Setup()
	scanner := bufio.NewScanner(os.Stdin)
	nuisance := 0.0
	for {
		now := time.Now()
		state := State{}
		for i := 0; i < 8; i++ {
			var colorA, colorB int
			scanner.Scan()
			line := scanner.Text()
			input(line)
			fmt.Sscan(line, &colorA, &colorB)
			state.Blocks = append(state.Blocks, [2]rune{rune(colorB + '0'), rune(colorA + '0')})
		}
		for i := 0; i < 12; i++ {
			var row string
			scanner.Scan()
			line := scanner.Text()
			input(line)
			fmt.Sscan(line, &row)
			for j := 0; j < 6; j++ {
				switch row[j] {
				case '.':
					state.MyStack[j][i] = Empty
				case '0':
					state.MyStack[j][i] = Blocker
					state.MyBlocks++
				default:
					state.MyStack[j][i] = rune(row[j])
					state.MyBlocks++
				}

			}
		}
		for i := 0; i < 12; i++ {
			// row: One line of the map ('.' = empty, '0' = skull block, '1' to '5' = colored block)
			var row string
			scanner.Scan()
			line := scanner.Text()
			input(line)
			fmt.Sscan(line, &row)
		}
		oppMinHeight := 13
		for i := 0; i < 6; i++ {
			h := height(&state.HisStack, i)
			if h < oppMinHeight {
				oppMinHeight = h
			}
		}
		oppState := state
		oppState.MyStack, oppState.HisStack = oppState.HisStack, oppState.MyStack
		hisBest := findMove(state, 2, 0, 0, 0)
		move := findMove(state, 0, nuisance, oppMinHeight, hisBest.Dropping)
		nuisance += float64(move.OwnScore) / 70
		//		printState(state)
		fmt.Fprintf(os.Stderr, "Moves: %s\nDrops: %d\nNuisance: %f\nOwnScore: %d\nStackBonus: %d\nHeightPen: %d\nColorPen: %d\nGroupScore: %d\nTotScore: %d\nChildScore:  %d\nDeleted: %d\nTime: %v\n",
			move.Movelist, move.Dropping, nuisance, move.OwnScore, move.StackBonus, move.HeightPen, move.ColorPen, move.GroupScore, move.Score, move.ChildScore, move.Deleted, time.Since(now))
		output(fmt.Sprintf("%d %d\n", move.Column, move.Rotation))
		for nuisance > 6 {
			nuisance -= 6
		}
	}
}

func main() {
	run()
}
